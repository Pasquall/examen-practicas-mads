# --- !Ups

-- Añadir tiempo de tarea

ALTER TABLE task ADD runningTime integer;

# --- !Downs

ALTER TABLE task DROP runningTime;