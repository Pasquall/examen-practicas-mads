import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import models.Task

import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */


class ModelSpec extends Specification {

  "Models" should {
      "create and find tasks" in {
         running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
            val taskId = Task.create("prueba","anonymous")
            val cero:Long = 0
            taskId must be_>=(cero)
         }
      }

      "the running time of a not started task must be 0" in {
         running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
            val taskId = Task.create("prueba","juan.perez")
            val runningTime = Task.runningTime(taskId)
            runningTime must be_==(0)
         }
      }

      "the running time after starting a task and before finishing it must be the spent time" in {
         running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
            val taskId = Task.create("prueba","juan.perez")
            Task.start(taskId);
            Thread.sleep(4000)
            val runningTime = Task.runningTime(taskId)
            runningTime must be_==(4)
         }
      }

      "the running time after finishing a task must be the spent time" in {
         running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
            val taskId = Task.create("prueba","juan.perez")
            Task.start(taskId);
            Thread.sleep(4000)
            Task.stop(taskId);
            val runningTime = Task.runningTime(taskId)
            runningTime must be_==(4)
         }
      }

      "the running time of task must be the spent time after the last start" in {
         running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
            val taskId = Task.create("prueba","juan.perez")
            Task.start(taskId);
            Thread.sleep(4000)
            Task.stop(taskId);
            Task.start(taskId);
            Thread.sleep(2000)
            val runningTime = Task.runningTime(taskId)
            runningTime must be_==(2)
         }
      }

  }
}
